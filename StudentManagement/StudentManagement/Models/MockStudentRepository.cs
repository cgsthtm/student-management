﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagement.Models
{
    public class MockStudentRepository : IStudentRepository
    {
        private List<Student> _students;
        public MockStudentRepository()
        {
            _students = new List<Student>()
            {
                new Student(){ Id=1,Name="张三",ClassName=ClassNameEnum.FirstGrade,Email="103070@qq.com" },
                new Student(){ Id=2,Name="李四",ClassName=ClassNameEnum.GradeThree,Email="828630@qq.com" },
                new Student(){ Id=3,Name="王五",ClassName=ClassNameEnum.None,Email="697415@qq.com" },
                new Student(){ Id=4,Name="赵六",ClassName=ClassNameEnum.SecondGrade,Email="236890@qq.com" },
            };
        }

        public IEnumerable<Student> GetAllStudents()
        {
            return _students;
        }

        public Student GetStudent(int id)
        {
            return _students.FirstOrDefault(x => x.Id == id);
        }

        public Student Add(Student student)
        {
            student.Id = _students.Max(s => s.Id) + 1;
            _students.Add(student);
            return student;
        }
    }
}
