using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StudentManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagement
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration) => _configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // AddMvcCoe只包含了核心的MVC功能
            // 而AddMvc包含了依赖于AddMvcCore以及相关的第三方常用的服务和方法
            services.AddMvc(options => //注册MVC服务
            {
                options.EnableEndpointRouting = false;
            });

            // 注册类型有三种：Singleton、Scoped和Transient
            services.AddSingleton<IStudentRepository, MockStudentRepository>();// 注册MockStudentRepository

            //解决Controller控制器返回的Json数据中文乱码问题
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //开发环境(development)、集成环境(integration)、测试环境(testing)、QA验证、模拟环境或预发布环境(staging)、生产环境(production)、UAT用户验收测试环境
            if (env.IsDevelopment())
            {
                DeveloperExceptionPageOptions developerExceptionPageOptions = new();
                developerExceptionPageOptions.SourceCodeLineCount = 10;//能看报错部分上下10行代码
                app.UseDeveloperExceptionPage(developerExceptionPageOptions);//开发者异常页面中间件
            }
            else if (env.IsProduction() || env.IsStaging() || env.IsEnvironment("UAT"))
            {
                //app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseRouting();
            //app.UseMvcWithDefaultRoute();//添加MVC默认路由
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });//自定义路由模板(传统路由),可以与属性路由相结合
            //app.UseMvc();//需要在控制器上使用属性路由

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapGet("/", async context =>
            //    {
            //        await context.Response.WriteAsync($"Hello World!");
            //    });
            //});

        }
    }
}
